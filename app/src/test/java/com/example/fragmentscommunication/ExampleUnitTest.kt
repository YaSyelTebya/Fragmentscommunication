package com.example.fragmentscommunication

import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    private lateinit var colorsRepository: FragmentsColorsRepository

    @Before
    fun createColorsRepository() {
        colorsRepository = FragmentsColorsRepository()
    }

    @Test
    fun `right after creating there are no colors`() {
        compareWithNullForEach(::assertEquals)
    }

    @Test
    fun `after updating method call there are some colors`() {
        colorsRepository.updateColors()
        compareWithNullForEach(::assertNotEquals)
    }

    @Test
    fun `swap method actually swaps`() {
        colorsRepository.updateColors()
        val frag1PreviousColor = colorsRepository.fragment1Color
        val frag2PreviousColor = colorsRepository.fragment2Color

        colorsRepository.swapColors()

        assertEquals(frag1PreviousColor, colorsRepository.fragment2Color)
        assertEquals(frag2PreviousColor, colorsRepository.fragment1Color)
    }

    private fun compareWithNullForEach(compareMethod: (Any?, ARGBColor?) -> Unit) {
        for (fragmentColor in
        listOf(colorsRepository.fragment1Color, colorsRepository.fragment2Color)) {
            compareMethod(null, fragmentColor)
        }
    }
}