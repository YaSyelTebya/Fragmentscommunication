package com.example.fragmentscommunication

import kotlin.random.Random

class FragmentsColorsRepository {
    var fragment1Color: ARGBColor? = null
        private set
    var fragment2Color: ARGBColor? = null
        private set

    fun updateColors() {
        fragment1Color = createRandomColor()
        fragment2Color = createRandomColor()
    }

    fun swapColors() {
        val tmpColorBuffer = fragment1Color
        fragment1Color = fragment2Color
        fragment2Color = tmpColorBuffer
    }

    private fun createRandomColor(): ARGBColor = Random.nextInt()
}