package com.example.fragmentscommunication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.fragmentscommunication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), ColorManipulator {
    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    private val colorsRepository: FragmentsColorsRepository by lazy {
        (application as FragmentsApplication).fragmentsColors
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        syncColors()
    }

    override fun changeColors() {
        colorsRepository.updateColors()
        syncColors()
    }

    override fun swapColors() {
        colorsRepository.swapColors()
        syncColors()
    }

    private fun syncColors() {
        with(binding) {
            colorsRepository.fragment1Color?.let { manipulativeFragment1.setBackgroundColor(it) }
            colorsRepository.fragment2Color?.let { manipulativeFragment2.setBackgroundColor(it) }
        }
    }
}

interface ColorManipulator {
    fun changeColors()

    fun swapColors()
}