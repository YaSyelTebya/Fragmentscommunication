package com.example.fragmentscommunication

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.fragmentscommunication.databinding.FragmentControllingBinding

class ControllingFragment : Fragment() {
    private var _binding: FragmentControllingBinding? = null
    private val binding get() = _binding!!
    private lateinit var activity: ColorManipulator

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as ColorManipulator
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentControllingBinding.inflate(inflater, container, false)

        setupButtons()

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupButtons() {
        with(binding) {
            changeColorsButton.setOnClickListener {
                activity.changeColors()
            }

            swapColorsButton.setOnClickListener {
                activity.swapColors()
            }
        }
    }
}