package com.example.fragmentscommunication

import android.app.Application

class FragmentsApplication : Application() {
    val fragmentsColors by lazy { FragmentsColorsRepository() }
}