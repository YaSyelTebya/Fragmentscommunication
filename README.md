# Android Fragment Activity
This repository contains an Android application that demonstrates the usage of fragments in an activity. The application consists of an activity that contains three fragments. The main functionality of the application includes changing the background color of Fragment2 and Fragment3, as well as swapping the positions of Fragment2 and Fragment3. Additionally, the application provides the option to save the state of the fragments after rotation, including their positions and background colors.

## Features
Fragment1:

Contains two buttons:
- Button 1: Changes the background color of Fragment2 and Fragment3. The colors can be randomly generated or can be set to the background color of Fragment2 for Fragment3 and vice versa.
- Button 2: Swaps the positions of Fragment2 and Fragment3.

Fragment2 and Fragment3:
- Each fragment has an identifying element, such as a label with the text "Fragment 2" and "Fragment 3" respectively.
State Persistence:

The application provides the option to save the state of the fragments after rotation. 

## Getting Started
To get started with the application, follow these steps:

Clone the repository to your local machine using the following command:
```bash
git clone https://gitlab.com/YaSyelTebya/Fragmentscommunication.git
```
Open the project in Android Studio.

Build and run the application on an Android device or emulator.